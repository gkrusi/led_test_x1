
#ifndef _LINEAR_INTERPOLATION_H
#define _LINEAR_INTERPOLATION_H
 
#ifdef __cplusplus
extern "C" {
#endif
  
#include <stdbool.h>
#include <math.h> 
#include <stdio.h> 
#include <stm32f30x.h>
  
  
#define UINT16_UNDEF 65535
#define FLOAT_MAX 999999999999999999
#define FLOAT_MIN -999999999999999999

#define MAX_NUM_OF_POINTS 25

	enum TransfareFunctionSlope
	{
		falling = -1,
		undefined = 0,
		rising = 1,
		error = 2
	};

	typedef struct 
	{
		float in = NAN;
		float out = NAN;
	} Point;

	typedef struct
	{  
		Point point[MAX_NUM_OF_POINTS+1];
		uint8_t nOfPoints = 0; 
	} TransferFunction;
	 
	 
	// external
	
	bool LinIntEditTransferFunction(TransferFunction *transF, uint8_t ID, float in, float out);
	bool LinearInterpolation(float input, float* output, TransferFunction transF); // return error

	// internal
	float LinEqSolve(float X1, float Y1, float X2, float Y2, float Y);

#ifdef __cplusplus
}
#endif 
#endif // _LINEAR_INTERPOLATION_H