
#include "filterGlitch.h"


void FilterGlitchInit(uint16_t _glitchMaxTime, uint16_t samplePeriod, FilterGlitchParameters *param)
{
	param ->glitchMaxTime = _glitchMaxTime;
	param->_glitchTime = 0;
	param ->_samplePeriod = samplePeriod;
	param ->valueUnglitched = false;
}

void FilterGlitchUpdate(bool newInput, FilterGlitchParameters *param)
{
	if (newInput != param->valueUnglitched)
	{
		if (param->_glitchTime > param->glitchMaxTime)
		{
			param->valueUnglitched = newInput;	// update value if no glitch detected for past glitchMaxTime
		}
		else
		{
			param->_glitchTime += param->_samplePeriod;	// count glitch free time
		}
	}
	else
	{
		param->_glitchTime = 0;	// restart counting if glitch detected
	}
}

bool FilterGlitchValue(FilterGlitchParameters *param)
{
	return param->valueUnglitched;
}