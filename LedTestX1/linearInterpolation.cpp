 
#ifdef __cplusplus
extern "C" {
#endif
	 
#include "linearInterpolation.h" 
 
/*
Linearly interpolate input point to transfare function and return it's interpolated output value as output. 

Example:

float interpolIn = 90;
float interpolOut;
TransferFunction transF; 
LinIntEditTransferFunction(&transF, 0, 0, 0);
LinIntEditTransferFunction(&transF, 1, 80, 500);
LinIntEditTransferFunction(&transF, 2, 100, 1000);  
LinearInterpolation(interpolIn, &interpolOut, transF);  

...interpolOut = 750 
*/

	bool LinIntEditTransferFunction(TransferFunction *transF, uint8_t ID, float in, float out)
	{
		if (ID > MAX_NUM_OF_POINTS) return true;	// out of range, increase MAX_NUM_OF_POINTS 

		// set point
		transF->point[ID].in = in;
		transF->point[ID].out = out;
		
		for (uint16_t i = 0; transF->point[i].in == transF->point[i].in && i < MAX_NUM_OF_POINTS; i++) 
			transF->nOfPoints = i+1;

		return false;	// ok
	}

	

	bool LinearInterpolation(float input, float* output, TransferFunction transF)
	{ 
		float smallerInVal = FLOAT_MIN; // 4.08964999e+018
		uint16_t smallerID = UINT16_UNDEF;
		float largerInVal = FLOAT_MAX; // -4.08964999e+018
		uint16_t largerID = UINT16_UNDEF; 
		
		// find ID of closest smaller input 
		for (uint16_t i = 0; i < transF.nOfPoints; i++) 
			if (transF.point[i].in <= input && transF.point[i].in >= smallerInVal)
			{
				smallerInVal = transF.point[i].in;
				smallerID = i;
			}
			
		// find ID of closest larger input
		for (uint16_t i = 0; i < transF.nOfPoints; i++) 
			if (transF.point[i].in >= input && transF.point[i].in <= largerInVal)
			{
				largerInVal = transF.point[i].in;
				largerID = i;
			}
			
		if (largerID == smallerID) { *output = transF.point[largerID].out; return false;}									// precise value
		if (largerID == UINT16_UNDEF && smallerID != UINT16_UNDEF) {*output = transF.point[smallerID].out; return true;}	// if out of range lower
		if (largerID != UINT16_UNDEF && smallerID == UINT16_UNDEF) {*output = transF.point[largerID].out; return true;}		// if out of range larger
		if (largerID == UINT16_UNDEF && smallerID == UINT16_UNDEF) {*output = 0; return true; }								// if out of range total, ~impossible
		
		*output = LinEqSolve(transF.point[smallerID].out, transF.point[smallerID].in, transF.point[largerID].out, transF.point[largerID].in, input);	// interpolate output value between smaller and larger value

		return false;	// ok
	} 

	 
	float LinEqSolve(float X1, float Y1, float X2, float Y2, float Y)
	{
		float X;
		float deltaX = X2 - X1;
		float deltaY = Y2 - Y1;
		float k = deltaY / deltaX;
		float n = Y1 - k * X1;
		X = (Y - n) / k;

		return X;
	} 


#ifdef __cplusplus
}
#endif  