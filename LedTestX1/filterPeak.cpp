
#include  "filterPeak.h"


void FilterPeakInit(uint16_t keepPeakPosTime, uint16_t keepPeakNegTime, uint16_t samplePeriod, FilterPeakParameters *param)
{
	param ->_keepPeakPosTime = keepPeakPosTime;
	param ->_keepPeakNegTime = keepPeakNegTime;
	param ->_samplePeriod = samplePeriod;
	param ->_peakNegTime = 0;
	param ->_peakPosTime = 0;
	param ->valuePeakNeg = 0;
	param ->valuePeakPos = 0; 
}

void FilterPeakUpdate(float newInput, FilterPeakParameters *param)
{
	// keep poistive peak
	if (newInput >= param->valuePeakPos)	// reset
	{
		param->_peakPosTime = 0;
		param->valuePeakPos = newInput;		// update value if it's larger
	}
	else
	{
		if (param->_peakPosTime >= param->_keepPeakPosTime)	// timeout
		{
			param->valuePeakPos  = newInput; // update value if timed out
		}				
		else
		{
			param->_peakPosTime += param->_samplePeriod;	// count time after peak detection
		}
	}
	
	// keep negative peak
	if (newInput <= param->valuePeakNeg)	// reset
	{
		param->_peakNegTime = 0;
		param->valuePeakNeg = newInput;		// update value if it's larger
	}
	else
	{
		if (param->_peakNegTime >= param->_keepPeakNegTime)	// timeout
		{
			param->valuePeakNeg  = newInput; // update value if timed out
		}				
		else
		{
			param->_peakNegTime += param->_samplePeriod;	// count time after peak detection
		}
	} 
}

float FilterPeakPositiveValue(FilterPeakParameters *param)
{
	return param->valuePeakPos;
}

float FilterPeakNegativeValue(FilterPeakParameters *param)
{
	return param->valuePeakNeg;
}
