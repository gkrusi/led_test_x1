
#ifndef __FILTER_PEAK_H
#define __FILTER_PEAK_H
     
#ifdef __cplusplus
extern "C" {
#endif
	 
#include <stdbool.h>
#include <math.h> 
#include <stdio.h> 

	    
	// create variable with type FilterPeakParameters, which represents filter
	// init it with FilterPeakInit
	// update it every samplePeriod time with call to FilterPeakUpdate
	// read value at any time with FilterPeakValue
	
	typedef struct
	{  
		uint16_t _peakPosTime;
		uint16_t _peakNegTime; 
		uint16_t _keepPeakPosTime;
		uint16_t _keepPeakNegTime; 
		uint16_t _samplePeriod;	// time ms 
		float valuePeakPos;
		float valuePeakNeg;
	} FilterPeakParameters;


	void FilterPeakInit(uint16_t keepPeakPosTime, uint16_t keepPeakNegTime, uint16_t samplePeriod, FilterPeakParameters *param);
	void FilterPeakUpdate(float newInput, FilterPeakParameters *param);
	float FilterPeakPositiveValue(FilterPeakParameters *param);
	float FilterPeakNegativeValue(FilterPeakParameters *param);

	
	
	
	 
#ifdef __cplusplus
}
#endif 
#endif	// __FILTER_PEAK_H
