 
#ifdef __cplusplus
extern "C" {
#endif
	 
#include "filterAverage.h" 

	
bool FilterAverageInit(uint8_t samplesToAverage, FilterAverageParameters *param)
{  
	// empty inputHistory
	for (uint8_t i = 0; i < MAX_NUM_OF_SAMPLES; i++)
	{
		param->_inputHistory[i] = 0;
	}

	if (samplesToAverage >= 1)
		if (samplesToAverage > MAX_NUM_OF_SAMPLES) 
		{ 
			param->samplesToAverage = MAX_NUM_OF_SAMPLES; 	 // error - overflow will ocour!
			return true; 
		}
		else
		param->samplesToAverage = samplesToAverage;
	else
		param->samplesToAverage = 1;
	
	return false;
}
	
void FilterAverageUpdate(float newInput, FilterAverageParameters *param)
{
	float SUM = 0;
	for (uint8_t i = 0; i < param->samplesToAverage-1; i++)
	{
		param->_inputHistory[i] = param->_inputHistory[i+1];
		SUM += param->_inputHistory[i];
	}
	param->_inputHistory[param->samplesToAverage-1] = newInput;
	SUM += param->_inputHistory[param->samplesToAverage-1];
	param ->value = SUM / (float)param->samplesToAverage;

	param->_downsampleCount++;
	if (param->_downsampleCount >= param->samplesToAverage)
	{
		param->_downsampleCount = 0;
		param->valueDownsampled = param->value;
	}
}
	
	float FilterAverageValue(FilterAverageParameters param)
	{
		return param.value;
	}

	float FilterAverageValueDownsampled(FilterAverageParameters param)
	{
		return param.valueDownsampled;
	}
 
	 

#ifdef __cplusplus
}
#endif  