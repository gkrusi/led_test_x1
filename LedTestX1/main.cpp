#include "main.h"
 
extern bool timerTick1ms;
extern bool timerTick5ms;
extern bool timerTick10ms;
extern bool timerTick20ms;
extern bool timerTick25ms;
extern bool timerTick50ms;
extern bool timerTick100ms;
extern bool timerTick250ms;
extern bool timerTick500ms;
extern bool timerTick1s;
extern bool timerTick5s;
extern bool timerTick60s;
extern uint16_t cycleTime;
extern uint16_t processingTime;
 
LedTestStruct ledTest;
 
StructIoPin gpioButton[N_OF_BUTTONS]; 
FilterGlitchParameters buttonFilter[N_OF_BUTTONS];
TLC_struct TLC;

int main()
{
	SysTick_Init(100);

	InitRCC(); 
	InitGPIO(&TLC, gpioButton);   
	InitTimer2(); 
	 
	ReadTlcErrors(&TLC);  

	for (uint8_t i = 0; i < N_OF_BUTTONS; i++)
	{
		FilterGlitchInit(100, 20, &buttonFilter[i]);
	}

	for (;;)	// infinite loops
	{
		// generate timer tick interrupts
		if (timerTick1ms)	{ timerTick1ms	 = false; TimerHandler1ms();   /* if (timerTick1ms)   if (!ignoreMainLoopTimeout) _MMX->errors.errorMainLoopTimeout = true; */ }
		if (timerTick5ms)	{ timerTick5ms	 = false; TimerHandler5ms();   /* if (timerTick5ms)   if (!ignoreMainLoopTimeout) _MMX->errors.errorMainLoopTimeout = true; */ }
		if (timerTick10ms)	{ timerTick10ms	 = false; TimerHandler10ms();  /* if (timerTick10ms)  if (!ignoreMainLoopTimeout) _MMX->errors.errorMainLoopTimeout = true;*/ }
		if (timerTick25ms)	{ timerTick25ms	 = false; TimerHandler25ms();  /* if (timerTick25ms)  if (!ignoreMainLoopTimeout) _MMX->errors.errorMainLoopTimeout = true;*/ }
																			 										   
		if (timerTick50ms)	{ timerTick50ms	 = false; TimerHandler50ms();  /* if (timerTick50ms)  if (!ignoreMainLoopTimeout) _MMX->errors.errorMainLoopTimeout = true; */ }
		if (timerTick100ms) { timerTick100ms = false; TimerHandler100ms(); /* if (timerTick100ms) if (!ignoreMainLoopTimeout) _MMX->errors.errorMainLoopTimeout = true;*/ }
		if (timerTick250ms) { timerTick250ms = false; TimerHandler250ms(); /* if (timerTick250ms) if (!ignoreMainLoopTimeout) _MMX->errors.errorMainLoopTimeout = true;*/ }
		if (timerTick500ms) { timerTick500ms = false; TimerHandler500ms(); /* if (timerTick500ms) if (!ignoreMainLoopTimeout) _MMX->errors.errorMainLoopTimeout = true;*/ }
		if (timerTick1s)	{ timerTick1s	 = false; TimerHandler1s();    /* if (timerTick1s)    if (!ignoreMainLoopTimeout) _MMX->errors.errorMainLoopTimeout = true; */ }
		if (timerTick5s)	{ timerTick5s	 = false; TimerHandler5s();    /* if (timerTick5s)    if (!ignoreMainLoopTimeout) _MMX->errors.errorMainLoopTimeout = true; */ }
		if (timerTick60s)	{ timerTick60s	 = false; TimerHandler60s();   /* if (timerTick60s)   if (!ignoreMainLoopTimeout) _MMX->errors.errorMainLoopTimeout = true; */ } 
		if (timerTick20ms)	{ timerTick20ms	 = false; TimerHandler20ms();  /* if (timerTick10ms)  if (!ignoreMainLoopTimeout) _MMX->errors.errorMainLoopTimeout = true;*/ }
		
		// read CAN messages
		CheckCANmessages();
	}
}
 

////////////////////////////////////////////////////////////////
////////////////////// timer interrupts ////////////////////////
//////////////////////////////////////////////////////////////// 
extern void TimerHandler1ms(void)
{ 
	ledTest.lastCycleTime = (float)cycleTime;
	cycleTime = 0;
} 


extern void TimerHandler5ms(void)
{

}

extern void TimerHandler10ms(void)
{
}
 
extern void TimerHandler20ms(void)
{  
	processingTime = 0;		

	// refresh buttons
	for (uint8_t i = 0; i < N_OF_BUTTONS; i++)
	{
		bool buttonRaw = !IoPin_ReadInput(gpioButton[i]);	// read inverted pin value
		FilterGlitchUpdate(buttonRaw, &buttonFilter[i]);	// process unglitching filter
		ledTest.buttonUnglitched[i] = FilterGlitchValue(&buttonFilter[i]);	// set new unglitched value
	}


	// set LED status
	for (uint8_t i = 0; i < N_OF_BUTTONS; i++)	// set button sorounding LED's regarding button status
	{
		if (ledTest.buttonUnglitched[i])	
		{
			// if button pressed
			ledTest.led[i * 5 + 0].R = 1;	// left side LED
			ledTest.led[i * 5 + 0].G = 1;
			ledTest.led[i * 5 + 0].B = 0;
			
			ledTest.led[i * 5 + 1].R = 1;	// left top LED
			ledTest.led[i * 5 + 1].G = 1;
			ledTest.led[i * 5 + 1].B = 1;
			
			ledTest.led[i * 5 + 2].R = 1;	// center top LED
			ledTest.led[i * 5 + 2].G = 1;
			ledTest.led[i * 5 + 2].B = 1;
			
			ledTest.led[i * 5 + 3].R = 1;	// right top LED
			ledTest.led[i * 5 + 3].G = 1;
			ledTest.led[i * 5 + 3].B = 1;
			
			ledTest.led[i * 5 + 4].R = 1;	// right side LED
			ledTest.led[i * 5 + 4].G = 1;
			ledTest.led[i * 5 + 4].B = 0;
		}
		else
		{	
			// if button not pressed
			ledTest.led[i * 5 + 0].R = 0;	// left side LED
			ledTest.led[i * 5 + 0].G = 0;
			ledTest.led[i * 5 + 0].B = 1;

			ledTest.led[i * 5 + 1].R = 0;	// left top LED
			ledTest.led[i * 5 + 1].G = 0;
			ledTest.led[i * 5 + 1].B = 0;

			ledTest.led[i * 5 + 2].R = 0;	// center top LED
			ledTest.led[i * 5 + 2].G = 0;
			ledTest.led[i * 5 + 2].B = 0;

			ledTest.led[i * 5 + 3].R = 0;	// right top LED
			ledTest.led[i * 5 + 3].G = 0;
			ledTest.led[i * 5 + 3].B = 0;

			ledTest.led[i * 5 + 4].R = 0;	// right side LED
			ledTest.led[i * 5 + 4].G = 0;
			ledTest.led[i * 5 + 4].B = 1;
		}
	} 



	// refresh LED's
	for (uint8_t tlc = 0; tlc < N_OF_TLCS; tlc++)	// print LED states to TLC outputs
	{
		for (uint8_t led = 0; led < 4; led++)
		{
			TLC.ledState[tlc*16+led*3 + 0] = ledTest.led[tlc*4+led].R;
			TLC.ledState[tlc*16+led*3 + 1] = ledTest.led[tlc*4+led].G;
			TLC.ledState[tlc*16+led*3 + 2] = ledTest.led[tlc*4+led].B;
		} 
	}  
	TlcRefresh(&TLC);	// refresh TLC outputs

	ledTest.lastProcessingTime = processingTime;	
}

extern void TimerHandler25ms(void)
{ 
    
}

extern void TimerHandler50ms(void)
{ 	
	
}

extern void TimerHandler100ms(void)
{ 

}

extern void TimerHandler250ms(void)
{  

}

extern void TimerHandler500ms(void)
{ 

} 

extern void TimerHandler1s(void)
{ 

}

extern void TimerHandler5s(void)
{ 

}

extern void TimerHandler60s(void)
{

}



