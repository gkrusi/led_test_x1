#ifndef __CALIBRATION_H
#define __CALIBRATION_H
     
#ifdef __cplusplus
extern "C" {
#endif
	  
	#include <stdio.h> 	
	
	struct structCalib
	{
		float _pointAin = 0;
		float _pointAout = 0;
		float _pointBin = 1;
		float _pointBout = 1;
		float coefK = 1;
		float coefN = 0;
		float outMin = 0;
		float outMax = 1;
	};
	
	typedef enum
	{ 
		CalibrationErrorNo,
		CalibrationErrorOverload,
		CalibrationErrorOutOfRangeMin,
		CalibrationErrorOutOfRangeMax
	} CalibrationError;

	void CalibrateShort(structCalib* calibration, float outMin, float outMax);
	void Calibrate(structCalib* calibration, float Ain, float Aout, float Bin, float Bout, float outMin, float outMax);
	CalibrationError CalculateCalibratedValue(structCalib calibration, float input, float* inputReturn);
	CalibrationError CheckCalibratedValueBoundrays(structCalib calibration, float calibratedValue);
	
	CalibrationError  CalculateCalibratedValueBounded(structCalib calibration, float input, float* inputReturn);
	CalibrationError BoundCalibratedValue(structCalib calibration, float* inputReturn);
	

	 

	   
#ifdef __cplusplus
}
#endif 
#endif	// __CALIBRATION_H


