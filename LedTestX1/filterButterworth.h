
#ifndef _FILTER_BUTTERWORTH_H
#define _FILTER_BUTTERWORTH_H
 
#ifdef __cplusplus
extern "C" {
#endif
	 
  
#include <stdbool.h>
#include <math.h> 
#include <stdio.h>  
  
	typedef enum 
	{  
		HighPass,
		LowPass,
	} PassType; 

	typedef struct
	{ 
		float _c, _a1, _a2, _a3, _b1, _b2;
		float _resonance;
		float _frequency;
		uint16_t _sampleRate;
		PassType _passType; 
		float _inputHistory[2];
		float _outputHistory[3]; 
		
		float value;				// filtered value
	} FilterButterworthParameters;

	void FilterButterworthInit(float frequency, uint16_t sampleRate, PassType passType, float resonance, FilterButterworthParameters *param);
	void FilterButterworthUpdate(float newInput, FilterButterworthParameters *param);
	float FilterButterworthValue(FilterButterworthParameters param);




#ifdef __cplusplus
}
#endif 
#endif // _FILTER_BUTTERWORTH_H