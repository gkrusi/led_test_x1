
#include "periphInit.h"   

uint16_t  ADC1ConvertedValue = 0; 

////////////////////////////////////////////////////////
////////////////////// RCC, NVIC ///////////////////////
////////////////////////////////////////////////////////
void InitRCC(void)
{ 
    /* Enable GPIO clock */ 
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE); 
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOE, ENABLE);
 
    /* Enable UART clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);  
	//RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	
}

////////////////////////////////////////////////////////
///////////////////////// GPIO /////////////////////////
//////////////////////////////////////////////////////// 
// communication interface pins
StructIoPin UART1_TX;
StructIoPin UART1_RX; 

void GpioInitAsOutput(StructIoPin *pin)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Pin = pin->GPIO_Pin; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(pin->GPIOx, &GPIO_InitStructure); 
} 

void GpioInitAsInput(StructIoPin *pin)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Pin = pin->GPIO_Pin;  
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN; 
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
	GPIO_Init(pin->GPIOx, &GPIO_InitStructure); 
}

 
bool InitGPIO(TLC_struct *tlc, StructIoPin button[])
{ 
	// init GPIO pin stuctures
	tlc->SDO.GPIOx = GPIOA;
	tlc->SDI.GPIOx = GPIOA;
	tlc->CLK.GPIOx = GPIOA;
	tlc->LE_ED1.GPIOx = GPIOA;
	tlc->OE_ED2.GPIOx = GPIOA;

	tlc->SDO.GPIO_Pin = GPIO_Pin_3;
	tlc->SDI.GPIO_Pin = GPIO_Pin_7;
	tlc->CLK.GPIO_Pin = GPIO_Pin_6;
	tlc->LE_ED1.GPIO_Pin = GPIO_Pin_5;
	tlc->OE_ED2.GPIO_Pin = GPIO_Pin_4;
	
	button[0].GPIOx = GPIOA;
	button[1].GPIOx = GPIOA;
	button[2].GPIOx = GPIOA;
	button[3].GPIOx = GPIOB;
	button[4].GPIOx = GPIOB;
	button[5].GPIOx = GPIOB;
	 
	button[0].GPIO_Pin = GPIO_Pin_0;
	button[1].GPIO_Pin = GPIO_Pin_1;
	button[2].GPIO_Pin = GPIO_Pin_2;
	button[3].GPIO_Pin = GPIO_Pin_12;
	button[4].GPIO_Pin = GPIO_Pin_13;
	button[5].GPIO_Pin = GPIO_Pin_14;

	// set output pins
	GpioInitAsOutput(&tlc->SDO);
	GpioInitAsOutput(&tlc->CLK);
	GpioInitAsOutput(&tlc->LE_ED1);
	GpioInitAsOutput(&tlc->OE_ED2); 

	// set input pins
	GpioInitAsInput(&tlc->SDI); 
	
	GpioInitAsInput(&button[0]); 
	GpioInitAsInput(&button[1]); 
	GpioInitAsInput(&button[2]); 
	GpioInitAsInput(&button[3]); 
	GpioInitAsInput(&button[4]); 
	GpioInitAsInput(&button[5]);

	return false;
}

bool InitDac(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	DAC_InitTypeDef DAC_InitStructure;
	  /* DAC Periph clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC1, ENABLE);
	
	  /* GPIO CONFIGURATION of DAC Pins */
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);


	/* DAC channel1 Configuration */
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_Init(DAC1, DAC_Channel_1, &DAC_InitStructure);

	/* Enable DAC Channel1 */
	DAC_Cmd(DAC1, DAC_Channel_1, ENABLE);
	
	return false;
}

bool InitAdc(void)
{
	GPIO_InitTypeDef	GPIO_InitStructure;
	ADC_InitTypeDef       ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;

	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  /* Configure the ADC clock */
	RCC_ADCCLKConfig(RCC_ADC12PLLCLK_Div2);
	/* Enable ADC2 clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC12, ENABLE);	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC34, ENABLE);	
	/* ADC Channel configuration */
	/* GPIOA Periph clock enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	  /* Configure ADC Channel0 as analog input */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
  
	ADC_StructInit(&ADC_InitStructure);

//	  /* Calibration procedure */  
//	ADC_VoltageRegulatorCmd(ADC1, ENABLE);
//  
//	/* Insert delay equal to 10 �s */
//	Delay_us(10);
//  
//	ADC_SelectCalibrationMode(ADC1, ADC_CalibrationMode_Single);
//	ADC_StartCalibration(ADC1);
//  
//	while (ADC_GetCalibrationStatus(ADC1) != RESET);
//	calibration_value = ADC_GetCalibrationValue(ADC1);
     
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;                                                                    
	ADC_CommonInitStructure.ADC_Clock = ADC_Clock_AsynClkMode;                    
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_2;             
	ADC_CommonInitStructure.ADC_DMAMode = ADC_DMAMode_Circular;                  
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = 250;          
	ADC_CommonInit(ADC1, &ADC_CommonInitStructure);
  
	ADC_InitStructure.ADC_ContinuousConvMode = ADC_ContinuousConvMode_Enable;
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b; 
	ADC_InitStructure.ADC_ExternalTrigConvEvent = ADC_ExternalTrigConvEvent_0;         
	ADC_InitStructure.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_OverrunMode = ADC_OverrunMode_Disable;   
	ADC_InitStructure.ADC_AutoInjMode = ADC_AutoInjec_Disable;  
	ADC_InitStructure.ADC_NbrOfRegChannel = 1;
	ADC_Init(ADC2, &ADC_InitStructure);
  
	/* ADC1 regular channel1 configuration */ 
	ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_2Cycles5);
   
	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);	
	
	return false;
}

uint16_t ADC_measureLight(void)
{
	// wait for ADRDY
	while (!ADC_GetFlagStatus(ADC1, ADC_FLAG_RDY))
		;
		
	// Start ADC1 Software Conversion
	ADC_StartConversion(ADC1);  		
		
	// Test EOC flag */
	while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET)
		;
    
	// Get ADC1 converted data
	return ADC_GetConversionValue(ADC1);
}

bool ReadInput(StructIoPin pin)
{ 
	return GPIO_ReadInputDataBit(pin.GPIOx, pin.GPIO_Pin); 
}

void SetOutput(StructIoPin pin, bool value)
{
	if (value == true)
	{
		GPIO_SetBits(pin.GPIOx, pin.GPIO_Pin);
	}
	else
	{
		GPIO_ResetBits(pin.GPIOx, pin.GPIO_Pin);
	}
}


////////////////////////////////////////////////////////
/////////////////////// TIMER 2 ////////////////////////
////////////////////////////////////////////////////////
uint8_t  count1ms   = 0;
uint8_t  count5ms   = 0;
uint8_t  count10ms  = 0;
uint8_t  count20ms  = 0;
uint8_t  count25ms  = 0;
uint8_t  count50ms  = 0;
uint8_t  count100ms = 0;
uint8_t  count250ms = 0;
uint16_t count500ms = 0;
uint16_t count1s    = 0;
uint16_t count5s    = 0;
uint16_t count60s   = 0; 
uint16_t cycleTime  = 0;
uint16_t processingTime = 0;

bool timerTick1ms   = false;
bool timerTick5ms   = false;
bool timerTick10ms  = false;
bool timerTick20ms  = false;
bool timerTick25ms  = false;
bool timerTick50ms  = false;
bool timerTick100ms = false;
bool timerTick250ms = false;
bool timerTick500ms = false;
bool timerTick1s    = false;
bool timerTick5s    = false;
bool timerTick60s   = false; 
  
bool InitTimer2(void)
{ 
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	/* Enable the TIM3 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	  /* TIM3 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	/* Time base configuration */

	TIM_TimeBaseStructure.TIM_Period = 2000 - 1;  // 1 MHz down to 1 KHz (1 ms)
	TIM_TimeBaseStructure.TIM_Prescaler = 36 - 1; // 24 MHz Clock down to 1 MHz (adjust per your clock)
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
	/* TIM IT enable */
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	/* TIM2 enable counter */
	TIM_Cmd(TIM2, ENABLE);
	
	return false;
}


extern "C" void TIM2_IRQHandler()	// set on 1 ms
{ 
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
		 
		cycleTime++;
		processingTime++;
		count1ms++;
		count5ms++;
		count10ms++;
		count20ms++;
		count25ms++;
		count50ms++;
		count100ms++;
		count250ms++;
		count500ms++;
		count1s++;
		count5s++;
		count60s++; 
		
		if (count1ms   >= 1) { timerTick1ms   = true; count1ms   = 0; }
		if (count5ms   >= 5) { timerTick5ms   = true; count5ms   = 0; }
		if (count10ms  >= 10) { timerTick10ms  = true; count10ms  = 0; }
		if (count20ms  >= 20) { timerTick20ms  = true; count20ms  = 0; }
		if (count25ms  >= 25) { timerTick25ms  = true; count25ms  = 0; }
		if (count50ms  >= 50) { timerTick50ms  = true; count50ms  = 0; }
		if (count100ms >= 100) { timerTick100ms = true; count100ms = 0; }
		if (count250ms >= 250) { timerTick250ms = true; count250ms = 0; }
		if (count500ms >= 500) { timerTick500ms = true; count500ms = 0; }
		if (count1s    >= 1000) { timerTick1s    = true; count1s    = 0; }
		if (count5s    >= 5000) { timerTick5s    = true; count5s    = 0; }
		if (count60s   >= 60000) { timerTick60s   = true; count60s   = 0; } 
	} 
}


////////////////////////////////////////////////////////
//////////////////////  CAN  1  ////////////////////////
////////////////////////////////////////////////////////	
//u8 canRx1MessageCount = 0;
//CanRxMsg CanRx1Message[100];

//class CanRx1Message
//{
//  // members are 'automatically' private by default
//	u8 canRx1MessageCount = 0;
//	CanRxMsg CanRx1Message[100];
// 
//    // you explicitely declare the public section
//public:
//  // ..
//	CanRxMsg get(){return CanRx1Message;}
////	void set(CanRxMsg CanRx1Messagen)
////	{
////		CanRx1Message = CanRx1Message; 
////	}
//};


bool InitCAN1(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef  NVIC_InitStructure;
	CAN_InitTypeDef        CAN_InitStructure;
	CAN_FilterInitTypeDef  CAN_FilterInitStructure;

  /* CAN GPIOs configuration **************************************************/

  /* Enable GPIO clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	  /* Connect CAN pins */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_9); //Rx
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource12, GPIO_AF_9); //Tx
  
	/* Configure CAN RX and TX pins */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	

	  /* CAN configuration ********************************************************/  
	  /* Enable CAN clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB, DISABLE);
	
  
	/* CAN register init */
	CAN_DeInit(CAN1);
	CAN_StructInit(&CAN_InitStructure);

	  /* CAN cell init */
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;  //!< Specifies the CAN operating mode.
	CAN_InitStructure.CAN_TTCM = DISABLE;    //!< Enable or disable the time triggered communication mode.
	CAN_InitStructure.CAN_ABOM = DISABLE;    //!< Enable or disable the automatic bus-off management.
	CAN_InitStructure.CAN_AWUM = DISABLE;    //!< Enable or disable the automatic wake-up mode.
	CAN_InitStructure.CAN_NART = DISABLE;     //!< Enable or disable the non-automatic retransmission mode.
	CAN_InitStructure.CAN_RFLM = DISABLE;   //!< Enable or disable the Receive FIFO Locked mode.
	CAN_InitStructure.CAN_TXFP = DISABLE;   //!< Enable or disable the transmit FIFO priority.
	
	
	CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;
   
	/* CAN Baudrate = 250kBps (CAN clocked at 42 MHz) */
	  /* Bitrate= CAN_CLK/(CAN_Prescaler * (1 + CAN_BS1 + CAN_BS2+CAN_SJW)) */
	  /*
	  	CAN_BS1 = 14 and CAN_BS2 = 6 
	  	Bitrate= 42000000/(CAN_Prescaler*(1+14+6+0))
	  	CAN_Prescaler=2 // 1000 kbit/s
	  	CAN_Prescaler=4 // 500 kbit/s
	  	CAN_Prescaler=5 // 400 kbit/s
	  	CAN_Prescaler=8 // 250 kbit/s
	  	CAN_Prescaler=10 // 200 kbit/s
	  	CAN_Prescaler=16 // 125 kbit/s
	  	CAN_Prescaler=20 // 100 kbit/s
	  	CAN_Prescaler=40 // 50 kbit/s
	  	CAN_Prescaler=80 // 40 kbit/s
	  	CAN_Prescaler=200 // 10 kbit/s
	  	*/
	CAN_InitStructure.CAN_BS1 = CAN_BS1_14tq; 
	CAN_InitStructure.CAN_BS2 = CAN_BS2_3tq;  //bilo 6 za STM32F407
	CAN_InitStructure.CAN_Prescaler = 8;
	CAN_Init(CAN1, &CAN_InitStructure);
	
	/* CAN filter init */


	CAN_FilterInitStructure.CAN_FilterNumber = 0; // CAN1 = 0 do 13
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;	
	CAN_FilterInitStructure.CAN_FilterIdLow =   0x0000;		
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;	
	CAN_FilterInitStructure.CAN_FilterMaskIdLow =  0x0000;	
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FIFO1;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);
		  
  
	/* Enable FIFO 1 message pending Interrupt */
	CAN_ITConfig(CAN1, CAN_IT_FMP1, ENABLE);
	
	  /* CAN Interrupt NVIC config */
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	 
	return false;
}