#ifndef __DEFINES_H
#define __DEFINES_H
     
#ifdef __cplusplus
extern "C" {
#endif
	 
#include <stm32f30x.h> 
 
	   


	    
#define N_OF_TLCS 6
#define N_OF_BITS_PER_TLC 16
#define N_OF_TLC_BITS (N_OF_TLCS*N_OF_BITS_PER_TLC)

#define N_OF_BUTTONS 6
#define N_OF_RGB_LEDS 30





#ifdef __cplusplus
}
#endif 
#endif