
#include "filterButterworth.h"




void FilterButterworthInit(float frequency, uint16_t sampleRate, PassType passType, float resonance, FilterButterworthParameters *param)
{   
	float PI = 3.14159265359;
	param ->_resonance = resonance;
	param ->_frequency = frequency;
	param ->_sampleRate = sampleRate;
	param ->_passType = passType;
	param ->value = 0;
	
	switch (param->_passType)
	{
	case LowPass:
		param->_c = (float)(1.0) / (float)tan(PI * param->_frequency / param->_sampleRate);
		param->_a1 = (float)(1.0) / ((float)(1.0f) + param->_resonance * param->_c + param->_c * param->_c);
		param->_a2 = (float)(2) * param->_a1;
		param->_a3 = param->_a1;
		param->_b1 = (float)(2.0) * ((float)(1.0) - param->_c * param->_c) * param->_a1;
		param->_b2 = ((float)(1.0) - param->_resonance * param->_c + param->_c * param->_c) * param->_a1;
		break;
	case HighPass:
		param->_c = (float)tan(PI * frequency / sampleRate);
		param->_a1 = (float)(1.0) / ((float)(1.0) + param->_resonance * param->_c + param->_c * param->_c);
		param->_a2 = (float)(-2) * param->_a1;
		param->_a3 = param->_a1;
		param->_b1 = (float)(2.0) * (param->_c * param->_c - (float)(1.0)) * param->_a1;
		param->_b2 = ((float)(1.0) - param->_resonance * param->_c + param->_c * param->_c) * param->_a1;
		break;
	default: break;
	}
} 

void FilterButterworthUpdate(float newInput, FilterButterworthParameters *param)
{
	float newOutput = param->_a1 * newInput + param->_a2 * param->_inputHistory[0] + param->_a3 * param->_inputHistory[1] - param->_b1 * param->_outputHistory[0] - param->_b2 * param->_outputHistory[1];

	param ->_inputHistory[1] = param->_inputHistory[0];
	param ->_inputHistory[0] = newInput;
		  
	param ->_outputHistory[2] = param->_outputHistory[1];
	param ->_outputHistory[1] = param->_outputHistory[0];
	param ->_outputHistory[0] = newOutput;
	param ->value = newOutput;
}

float FilterButterworthValue(FilterButterworthParameters param)
{
	return param._outputHistory[0];
}
				
				
				
				
