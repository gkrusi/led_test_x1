
 
#ifdef __cplusplus
extern "C" {
#endif
	 
#include "CANhandling.h" 
	
	CANfifoRx CANfifoRx1;
	CANfifoRx CANfifoRx2; 
	
	CANfifoTx CANfifoTx1;
	CANfifoTx CANfifoTx2; 
 
	uint8_t tx0;
	uint8_t tx1;
	uint8_t tx2;
	
	bool CANfifoAddRx(CANfifoRx *fifo, CanRxMsg *msg)
	{
		// caunt messages in FIFO
		if (fifo->_CANfifoCount >= CAN_FIFO_LENGHT - 1) return true;	// overflow 
		fifo->_CANfifoCount++; 
		
		if (fifo->_CAN_fifoWriteOffset >= CAN_FIFO_LENGHT - 1) fifo->_CAN_fifoWriteOffset = 0;
		else fifo->_CAN_fifoWriteOffset++;
		
		// write message to FIFO
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].DLC   = msg->DLC;
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].ExtId = msg->ExtId;
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].FMI   = msg->FMI;
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].IDE   = msg->IDE;
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].RTR   = msg->RTR;
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].StdId = msg->StdId;
		
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[0] = msg->Data[0];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[1] = msg->Data[1];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[2] = msg->Data[2];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[3] = msg->Data[3];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[4] = msg->Data[4];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[5] = msg->Data[5];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[6] = msg->Data[6];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[7] = msg->Data[7];
		
		return false;
	}
	
	
	bool CANfifoAddTx(CANfifoTx *fifo, CanTxMsg *msg)
	{
		// caunt messages in FIFO
		if (fifo->_CANfifoCount >= CAN_FIFO_LENGHT - 1) return true;	// overflow 
		fifo->_CANfifoCount++; 
		
		if (fifo->_CAN_fifoWriteOffset >= CAN_FIFO_LENGHT - 1) fifo->_CAN_fifoWriteOffset = 0;
		else fifo->_CAN_fifoWriteOffset++;
		
		// write message to FIFO
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].DLC   = msg->DLC;
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].ExtId = msg->ExtId;
		//fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].FMI   = msg->FMI;
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].IDE   = msg->IDE;
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].RTR   = msg->RTR;
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].StdId = msg->StdId;
		
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[0] = msg->Data[0];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[1] = msg->Data[1];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[2] = msg->Data[2];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[3] = msg->Data[3];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[4] = msg->Data[4];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[5] = msg->Data[5];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[6] = msg->Data[6];
		fifo->_CANfifo[fifo->_CAN_fifoWriteOffset].Data[7] = msg->Data[7];
		
		msg->Data[0] = 0;
		msg->Data[1] = 0;
		msg->Data[2] = 0;
		msg->Data[3] = 0;
		msg->Data[4] = 0;
		msg->Data[5] = 0;
		msg->Data[6] = 0;
		msg->Data[7] = 0;
		
		return false;
	}
	
	uint8_t CANfifoCountRx(CANfifoRx *fifo)
	{
		return fifo->_CANfifoCount;
	}
	uint8_t CANfifoCountTx(CANfifoTx *fifo)
	{
		return fifo->_CANfifoCount;
	}
	
	CanRxMsg CANfifoReadRx(CANfifoRx *fifo)
	{
		CanRxMsg msg;
		
		// caunt messages in FIFO
		if (fifo->_CANfifoCount == 0) return msg;	// fifo is empty, return empty message!
		fifo->_CANfifoCount--;
		
		if (fifo->_CAN_fifoReadOffset >= CAN_FIFO_LENGHT - 1) fifo->_CAN_fifoReadOffset = 0;
		else fifo->_CAN_fifoReadOffset++;
		
		// return next message 
		return fifo->_CANfifo[fifo->_CAN_fifoReadOffset];
	}
	
	
	CanTxMsg CANfifoReadTx(CANfifoTx *fifo)
	{
		CanTxMsg msg;
		
		// caunt messages in FIFO
		if (fifo->_CANfifoCount == 0) return msg;	// fifo is empty, return empty message!
		fifo->_CANfifoCount--;
		
		if (fifo->_CAN_fifoReadOffset >= CAN_FIFO_LENGHT - 1) fifo->_CAN_fifoReadOffset = 0;
		else fifo->_CAN_fifoReadOffset++;
		
		// return next message 
		return fifo->_CANfifo[fifo->_CAN_fifoReadOffset];
	}
	
	bool CheckCANmessages(void) 
	{
		// check CAN 1
		while (CANfifoCountRx(&CANfifoRx1) != 0)
		{
			CanRxMsg msg = CANfifoReadRx(&CANfifoRx1);
			//CAN1_MessageReceived(&msg);
		}
		return false;
	}	
	
	
	void CAN_RX1_IRQHandler(void)
	{
		CanRxMsg msg;
		CAN_Receive(CAN1, CAN_FIFO1, &msg);
		CANfifoAddRx(&CANfifoRx1, &msg);
		CAN_ClearITPendingBit(CAN1, CAN_IT_FMP1);
	}
	
	void CAN1_TX_IRQHandler(void)   // CAN1 TRANSMITT INTERRUPT 
	{
		CAN_ClearITPendingBit(CAN1, CAN_IT_TME);
		CAN1_StartTransmission();
	} 
	 
	
	bool CAN1_StartTransmission(void)
	{
		if (CANfifoCountTx(&CANfifoTx1) > 0)
		{
			tx0 = CAN_TransmitStatus(CAN1, 0);
			tx1 = CAN_TransmitStatus(CAN1, 1);
			tx2 = CAN_TransmitStatus(CAN1, 2); 
			if (tx0 == CAN_TxStatus_Pending && tx1 == CAN_TxStatus_Pending & tx2 == CAN_TxStatus_Pending) return true;	// send buffer is full 
			CanTxMsg TxMessage = CANfifoReadTx(&CANfifoTx1);
			CAN_Transmit(CAN1, &TxMessage);
		}
		return false;
	}
	
	bool CAN1_MessageTransmit(CanTxMsg *msg)
	{
		// add message to FIFO
		if (CANfifoAddTx(&CANfifoTx1, msg)) return true;	// return error if overflow occurred
		
		// request sending
		 
		return false;
	}


	
	
	////////////////////////////////////////////////////////////////////////
	////////////////////////////    generic    /////////////////////////////
	////////////////////////////////////////////////////////////////////////
	bool BitGet(uint16_t var, uint8_t bit) 
	{
		uint16_t mask = 0x01;
		mask = mask << bit;
		if ((var&mask) > 0){ return true; }
		return false;
	}

	void BitSet(uint8_t *var, uint8_t bit, bool state) 
	{
		if (state) {
			*var |=  (0x01 << bit);  // set
		}
		else {
			*var &= ~(0x01 << bit);  // clr
		}
	}


	///////////////////////////////////////////////////////////////
	////////////////////// CAN package handler ////////////////////
	///////////////////////////////////////////////////////////////
	
	void CanSetId(CanTxMsg* packet, CanIdLenght idLenght, uint8_t DLC, uint32_t ID)
	{
		packet->Data[0] = 0x00;
		packet->Data[1] = 0x00;
		packet->Data[2] = 0x00;
		packet->Data[3] = 0x00;
		packet->Data[4] = 0x00;
		packet->Data[5] = 0x00;
		packet->Data[6] = 0x00;
		packet->Data[7] = 0x00;
		packet->DLC = DLC;					// set number of bytes in CAN message
		packet->IDE = (uint8_t)idLenght;
		if (idLenght == canId11bit)
		{
			packet->StdId = ID;
			packet->ExtId = 0;
		}
		else
		{
			packet->StdId = 0;
			packet->ExtId = ID;
		}
	}
	
	
	bool CanSetByte(CanTxMsg* packet, uint8_t byteOffset, uint8_t value)
	{
		if (byteOffset >= 8) return true; 
		packet->Data[byteOffset] = value;
		return false;
	}

	bool CanSetBit(CanTxMsg* packet, uint8_t byteOffset, uint8_t bitOffset, bool value)
	{
		if (byteOffset >= 8) return true;
		if (bitOffset >= 8) return true;
		BitSet(&packet->Data[byteOffset], bitOffset, value);
		return false;
	}

	bool CanSetFloat(CanTxMsg* packet, uint8_t byteOffset, float value)
	{
		FLOATUNION_CAN myFloat;
		if (byteOffset > 4) return true; 
		myFloat.number = value;
		packet->Data[byteOffset] = myFloat.bytes[0]; 
		packet->Data[byteOffset + 1] = myFloat.bytes[1]; 
		packet->Data[byteOffset + 2] = myFloat.bytes[2]; 
		packet->Data[byteOffset + 3] = myFloat.bytes[3];  
		return false;
	}

	bool CanSetUint16(CanTxMsg* packet, uint8_t byteOffset, uint16_t value)
	{
		if (byteOffset > 6) return true; 
		packet->Data[byteOffset] = (uint8_t)(value >> 8);
		packet->Data[byteOffset + 1] = (uint8_t)value; 
		return false;
	}

 

	bool CanSetUint32(CanTxMsg* packet, uint8_t byteOffset, uint32_t value)
	{ 
		if (byteOffset > 4) return true; 

		packet->Data[byteOffset] = (uint8_t)(value >> 24);
		packet->Data[byteOffset + 1] = (uint8_t)(value >> 16);
		packet->Data[byteOffset + 2] = (uint8_t)(value >> 8);
		packet->Data[byteOffset + 3] = (uint8_t)value; 
		return false;
	}

	
	CanIdLenght CanGetIdLenght(CanRxMsg* packet)
	{
		return (CanIdLenght)packet->IDE;	
	}
	
	uint32_t CanGetId(CanRxMsg* packet)
	{
		if ((CanIdLenght)packet->IDE == canId11bit)
			return packet->StdId;
		else
			return packet->ExtId;
	}
	
	
	uint8_t CanGetByte(CanRxMsg* packet, uint8_t byteOffset)
	{ 
		return packet->Data[byteOffset]; 
	}

	bool CanGetBit(CanRxMsg* packet, uint8_t byteOffset, uint8_t bitOffset)
	{
		return BitGet(packet->Data[byteOffset], bitOffset);
	}

	float CanGetFloat(CanRxMsg* packet, uint8_t byteOffset)
	{
		FLOATUNION_CAN myFloat;
		myFloat.bytes[0] = packet->Data[byteOffset];
		myFloat.bytes[1] = packet->Data[byteOffset + 1];
		myFloat.bytes[2] = packet->Data[byteOffset + 2];
		myFloat.bytes[3] = packet->Data[byteOffset + 3];
		return myFloat.number;
	}

	uint16_t CanGetUint16(CanRxMsg* packet, uint8_t byteOffset)
	{ 	
		return (uint16_t)(packet->Data[byteOffset] << 8) + (uint16_t)packet->Data[byteOffset + 1];
	}

	uint32_t CanGetUint32(CanRxMsg* packet, uint8_t byteOffset) 
	{  	
		return (uint32_t)(packet->Data[byteOffset] << 24) + (uint32_t)(packet->Data[byteOffset + 1] << 16) + (uint32_t)(packet->Data[byteOffset + 2] << 8) + (uint32_t)packet->Data[byteOffset + 3] ;
	} 

	
	
#ifdef __cplusplus
}
#endif  