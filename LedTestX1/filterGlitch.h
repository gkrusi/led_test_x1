
#ifndef _FILTER_GLITCH_H
#define _FILTER_GLITCH_H
 
#ifdef __cplusplus
extern "C" {
#endif
	  
	#include <stdbool.h>
	#include <math.h> 
	#include <stdio.h>  
	    
	 
	
	typedef struct
	{  
		uint16_t glitchMaxTime = 500;	// max glitch time not to toggle output
		uint16_t _glitchTime = 0;		// internal current glitch time value
		uint16_t _samplePeriod = 25;	// time ms 
		bool valueUnglitched;			// current output value
	} FilterGlitchParameters;


	void FilterGlitchInit(uint16_t _glitchMaxTime, uint16_t samplePeriod, FilterGlitchParameters *param);
	void FilterGlitchUpdate(bool newInput, FilterGlitchParameters *param);
	bool FilterGlitchValue(FilterGlitchParameters *param);
	
	
	

#ifdef __cplusplus
}
#endif 
#endif // _FILTER_GLITCH_H