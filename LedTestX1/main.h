#ifndef __MAIN_H
#define __MAIN_H
     
#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f30x.h"
	
#include <stm32f30x_gpio.h>
#include <stm32f30x_rcc.h>
#include <stdio.h>
//#include "stm32f3_delay.h"
/***********************************************/
#include "stm32f30x_adc.h"
#include "stm32f30x_can.h"
#include "stm32f30x_comp.h"
#include "stm32f30x_dbgmcu.h"
#include "stm32f30x_dma.h"
#include "stm32f30x_exti.h"
#include "stm32f30x_flash.h"
#include "stm32f30x_gpio.h"
#include "stm32f30x_syscfg.h"
#include "stm32f30x_i2c.h"
#include "stm32f30x_iwdg.h"
#include "stm32f30x_opamp.h"
#include "stm32f30x_pwr.h"
#include "stm32f30x_rcc.h"
#include "stm32f30x_rtc.h"
#include "stm32f30x_spi.h"
#include "stm32f30x_tim.h"
#include "stm32f30x_usart.h"
#include "stm32f30x_wwdg.h"
#include "stm32f30x_misc.h"  /* High level functions for NVIC and SysTick (add-on to CMSIS functions) */
 
#include "periphInit.h"
#include "CANhandling.h" 
#include "delay.h" 	
#include "defines.h" 
#include "ledDisplay.h"
#include "filterGlitch.h"

	
	
///// timer interrupts /////
void TimerHandler1ms(void);
void TimerHandler5ms(void);
void TimerHandler10ms(void);
void TimerHandler20ms(void);
void TimerHandler25ms(void);
void TimerHandler50ms(void);
void TimerHandler100ms(void);
void TimerHandler250ms(void);
void TimerHandler500ms(void);
void TimerHandler1s(void);
void TimerHandler5s(void);
void TimerHandler60s(void);
	  
///// new message received on CAN ///// 
extern void CAN1_MessageReceived(CanRxMsg *msg);

void FunkcijaTLC(bool data[]); 
	 
		
#ifdef __cplusplus
}
#endif 
#endif	// __MAIN_H









