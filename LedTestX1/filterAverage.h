
#ifndef _FILTER_AVERAGE_H
#define _FILTER_AVERAGE_H
 
#ifdef __cplusplus
extern "C" {
#endif
  
#include <stdbool.h>
#include <math.h> 
#include <stdio.h>  
  
#define MAX_NUM_OF_SAMPLES 40

typedef struct
{ 
	uint8_t samplesToAverage;
	uint8_t _downsampleCount;
	float _inputHistory[MAX_NUM_OF_SAMPLES];
	float value;				// filtered value
	float valueDownsampled;		// downsampled value (averaged every specified amount of time
} FilterAverageParameters;

bool FilterAverageInit(uint8_t samplesToAverage, FilterAverageParameters *param);
void FilterAverageUpdate(float newInput, FilterAverageParameters *param);
float FilterAverageValue(FilterAverageParameters param);
float FilterAverageValueDownsampled(FilterAverageParameters param);
 
	
#ifdef __cplusplus
}
#endif 
#endif // _FILTER_AVERAGE_H