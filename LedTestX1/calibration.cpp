
#include "calibration.h"
 

void Calibrate(structCalib* calibration, float Ain, float Aout, float Bin, float Bout, float outMin, float outMax)
{
	calibration->coefK = (Bout - Aout) / (Bin - Ain);
	calibration->coefN = Aout - calibration->coefK * Ain;
	calibration->_pointAin = Ain;
	calibration->_pointAout = Aout;
	calibration->_pointBin = Bin;
	calibration->_pointBout = Bout;
	calibration->outMin = outMin;	// set upper limit
	calibration->outMax = outMax;	// set lower limit
}

 

void CalibrateShort(structCalib* calibration, float outMin, float outMax)
{
	calibration->coefK = (calibration->_pointBout - calibration->_pointAout) / (calibration->_pointBin - calibration->_pointAin);
	calibration->coefN = calibration->_pointAout - calibration->coefK * calibration->_pointAin;
	calibration->outMin = outMin;	// set upper limit
	calibration->outMax = outMax;	// set lower limit
}

CalibrationError CalculateCalibratedValue(structCalib calibration, float input, float* inputReturn)
{
	*inputReturn = calibration.coefK * input + calibration.coefN;
	return CheckCalibratedValueBoundrays(calibration, *inputReturn);
}

CalibrationError CalculateCalibratedValueBounded(structCalib calibration, float input, float* inputReturn)
{
	*inputReturn = calibration.coefK * input + calibration.coefN;
	return BoundCalibratedValue(calibration, inputReturn);
}

CalibrationError CheckCalibratedValueBoundrays(structCalib calibration, float calibratedValue)
{
	if (calibration.outMax > calibration.outMin)
	{
		if (calibratedValue > calibration.outMax) return CalibrationErrorOutOfRangeMax;
		if (calibratedValue < calibration.outMin) return CalibrationErrorOutOfRangeMin;
	}
	else
	{
		if (calibratedValue < calibration.outMax) return CalibrationErrorOutOfRangeMin;
		if (calibratedValue > calibration.outMin) return CalibrationErrorOutOfRangeMax;
	}
	return CalibrationErrorNo;
}

CalibrationError BoundCalibratedValue(structCalib calibration, float* inputReturn)
{
	if (calibration.outMax > calibration.outMin)
	{
		if (*inputReturn > calibration.outMax) {*inputReturn = calibration.outMax; return CalibrationErrorOutOfRangeMax;}
		if (*inputReturn < calibration.outMin) {*inputReturn = calibration.outMin; return CalibrationErrorOutOfRangeMin;}
	}
	else
	{
		if (*inputReturn < calibration.outMax) {*inputReturn = calibration.outMax; return CalibrationErrorOutOfRangeMin;}
		if (*inputReturn > calibration.outMin) {*inputReturn = calibration.outMin; return CalibrationErrorOutOfRangeMax;}
	}
	return CalibrationErrorNo;
}

