#ifndef __LED_DISPLAY_H
#define __LED_DISPLAY_H
     
#ifdef __cplusplus
extern "C" {
#endif

#include "CANhandling.h" 
#include "stm32f30x.h"
#include "font_10_14.h"
#include "delay.h"
#include "defines.h"
#include <stdio.h>
#include "calibration.h"
#include "linearInterpolation.h" 
#include "ioPin.h"

	struct LedRgbStruct
	{
		bool R = false;
		bool G = false;
		bool B = false;
	};
	
	struct TLC_struct
	{ 
		StructIoPin SDO; 
		StructIoPin SDI; 
		StructIoPin CLK;
		StructIoPin LE_ED1;
		StructIoPin OE_ED2; 
		bool ledState[N_OF_TLC_BITS];	
		bool ledError[N_OF_TLC_BITS];
	};
	 

	struct LedTestStruct
	{
		bool buttonUnglitched[N_OF_BUTTONS];
		LedRgbStruct led[N_OF_RGB_LEDS]; 
		float lastCycleTime = 0;
		float lastProcessingTime = 0;
	};


	

	 
	void TlcRefresh(TLC_struct *tlc);

	void ReadTlcErrors(TLC_struct *tlc);								// 
	void DisplayFull(bool state, uint8_t nOfChips, TLC_struct *tlc);	// 
	bool TlcData(bool OE_ED2, bool LE_ED1, bool SDI, TLC_struct *tlc);	//   
	  
	

#ifdef __cplusplus
}
#endif 
#endif	// __LED_DISPLAY_H