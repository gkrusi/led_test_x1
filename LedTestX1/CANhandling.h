
#ifndef _CAN_HANDLING_H
#define _CAN_HANDLING_H
 
#ifdef __cplusplus
extern "C" {
#endif
    
#include <stm32f30x.h>  
#include <stm32f30x_can.h>

  
#define CAN_FIFO_LENGHT 100
	
	
	enum CanIdLenght
	{
		canId11bit = CAN_ID_STD,
		canId29bit = CAN_ID_EXT
	};
	
	typedef struct
	{ 
		CanRxMsg _CANfifo[CAN_FIFO_LENGHT];
		uint8_t _CANfifoCount;
		uint8_t _CAN_fifoReadOffset;
		uint8_t _CAN_fifoWriteOffset;
	} CANfifoRx;
 
	
	typedef struct
	{ 
		CanTxMsg _CANfifo[CAN_FIFO_LENGHT];
		uint8_t _CANfifoCount;
		uint8_t _CAN_fifoReadOffset;
		uint8_t _CAN_fifoWriteOffset;
	} CANfifoTx;
	
	bool CANfifoAddRx(CANfifoRx *fifo, CanRxMsg *msg);
	bool CANfifoAddTx(CANfifoTx *fifo, CanTxMsg *msg);
	uint8_t CANfifoCountRx(CANfifoRx *fifo);
	uint8_t CANfifoCountTx(CANfifoTx *fifo);
	CanRxMsg CANfifoReadRx(CANfifoRx *fifo);
	CanTxMsg CANfifoReadTx(CANfifoTx *fifo);
	
	bool CheckCANmessages(void);
	void CAN1_MessageReceived(CanRxMsg *msg); 
	
	bool CAN1_MessageTransmit(CanTxMsg *msg);
	bool CAN1_StartTransmission(void);
	 
	extern "C" void CAN_RX1_IRQHandler(void);   // CAN1 RECEIVE INTERRUPT
	extern "C" void CAN1_TX_IRQHandler(void);   // CAN1 TRANSMITT INTERRUPT
		 
	
	 
	 
	////////////////////////////////////////////////////////////////////////
	////////////////////////////    generic    /////////////////////////////
	////////////////////////////////////////////////////////////////////////
	bool BitGet(uint16_t var, uint8_t bit);
	void BitSet(uint8_t *var, uint8_t bit, bool state);
	 
	 

	 
	///////////////////////////////////////////////////////////////
	////////////////////// CAN package handler ////////////////////
	///////////////////////////////////////////////////////////////
	typedef union
	{
		float number;
		uint8_t bytes[4];
	} FLOATUNION_CAN;

	void CanSetId(CanTxMsg* packet, CanIdLenght idLenght, uint8_t DLC, uint32_t ID);
	bool CanSetByte(CanTxMsg* packet, uint8_t byteOffset, uint8_t value);
	bool CanSetBit(CanTxMsg* packet, uint8_t byteOffset, uint8_t bitOffset, bool value);
	bool CanSetFloat(CanTxMsg* packet, uint8_t byteOffset, float value);
	bool CanSetUint16(CanTxMsg* packet, uint8_t byteOffset, uint16_t value);
	bool CanSetUint32(CanTxMsg* packet, uint8_t byteOffset, uint32_t value);
	 
	CanIdLenght CanGetIdLenght(CanRxMsg* packet);
	uint32_t CanGetId(CanRxMsg* packet);
	uint8_t CanGetByte(CanRxMsg* packet, uint8_t byteOffset);
	bool CanGetBit(CanRxMsg* packet, uint8_t byteOffset, uint8_t bitOffset);
	float CanGetFloat(CanRxMsg* packet, uint8_t byteOffset);
	uint16_t CanGetUint16(CanRxMsg* packet, uint8_t byteOffset);
	uint32_t CanGetUint32(CanRxMsg* packet, uint8_t byteOffset);
#ifdef __cplusplus
}
#endif 
#endif // _CAN_HANDLING_H