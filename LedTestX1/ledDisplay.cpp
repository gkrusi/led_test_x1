#include "ledDisplay.h"


 
void TlcRefresh(TLC_struct *tlc)
{
  	// switch to normal mode 
	IoPin_SetOutput(tlc->OE_ED2, false);	// latch data

	for (u16 i = 0; i < N_OF_TLC_BITS; i++)
	{
		if ( tlc->ledState[i]) IoPin_SetOutput(tlc->SDI, true); //data
		else IoPin_SetOutput(tlc->SDI, false);	//data
		IoPin_SetOutput(tlc->CLK, true);			//clk=1
		Delay_us(1);
		IoPin_SetOutput(tlc->CLK, false);			//clk=0
		Delay_us(1);
	}
	IoPin_SetOutput(tlc->LE_ED1, true);			// latch data
	Delay_us(1);
	IoPin_SetOutput(tlc->LE_ED1, false);
	Delay_us(1);

}

void ReadTlcErrors(TLC_struct *tlc)
{ 
	DisplayFull(true, N_OF_TLCS, tlc);

	// switch to special mode 
	TlcData(true , false, false, tlc);
	TlcData(false, false, false, tlc);
	TlcData(true, false, false, tlc);
	TlcData(true, true, false, tlc);
	TlcData(true, false, false, tlc);

	// read errors
	TlcData(true, false, false, tlc);
	//Delay_us(2);
	TlcData(false, false, false, tlc);
	//Delay_us(2);
	TlcData(false, false, false, tlc);
	//Delay_us(2);
	TlcData(false, false, false, tlc);
	Delay_us(4);

	for (u16 i = 0; i < N_OF_TLC_BITS; i++)
	{
		tlc->ledError[i] = TlcData(true, false, false, tlc);
	}
	
	DisplayFull(true, N_OF_TLCS, tlc);
}


void DisplayFull(bool state, uint8_t nOfChips, TLC_struct *tlc)		// turn whole display on or off
{
  	// switch to normal mode 
	TlcData(true, false, false, tlc);
	TlcData(false, false, false, tlc);
	TlcData(true, false, false, tlc);
	TlcData(true, false, false, tlc);
	TlcData(true, false, false, tlc);

	// turn led's on
	for (u16 i = 0; i < nOfChips * 16; i++)
	{
		TlcData(false, false, state, tlc);
	}
 
	IoPin_SetOutput(tlc->LE_ED1, true);	// latch data 
	IoPin_SetOutput(tlc->LE_ED1, false); 
}



bool TlcData(bool OE_ED2, bool LE_ED1, bool SDI, TLC_struct *tlc)
{ 
	IoPin_SetOutput(tlc->CLK, false);	 

	IoPin_SetOutput(tlc->SDI, SDI);	
	IoPin_SetOutput(tlc->LE_ED1, LE_ED1);	 
	IoPin_SetOutput(tlc->OE_ED2, OE_ED2); 

	//Delay_us(1);
	bool SDO = IoPin_ReadInput(tlc->SDO);
	IoPin_SetOutput(tlc->CLK, true);
	//Delay_us(1);
	IoPin_SetOutput(tlc->CLK, false);	 	
	
	return SDO;
}
 